function sortByModels(inventory) {
    let sortedArr = [];
    if(!Array.isArray(inventory) || inventory.length === 0)
    {
        const arr = [];
        return arr;
    }

    for(let i =0; i < inventory.length; i++)
    {
        let carModel = inventory[i].car_model;
        sortedArr.push(carModel);
    }

    return sortedArr.sort();
}

module.exports = sortByModels;