
 function findCarByID(inventory,carID){
    if(!Array.isArray(inventory) || inventory.length === 0 || carID === undefined) {
        const arr = [];
        return arr;
    }

    for(let i =0; i < inventory.length; i++)
    {
        let car = inventory[i];
        if(car["id"] === carID)
        {   let output = car;
            // let output = `car is a ${car.car_year} ${car.car_make} ${car.car_model}`;
            return output;
        }
    }
}


module.exports = findCarByID;