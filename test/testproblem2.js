const problem2 = require('../problem2');
const inventory = require('../cars.js');

let car = problem2(inventory);
console.log(`Last car is a ${car.car_make} ${car.car_model}`);